FROM golang

MAINTAINER Daniil Zh-v

ENV HOME /root

RUN apt-get update -qq

RUN mkdir -p /app
WORKDIR /app
COPY . /app/
RUN go get github.com/streadway/amqp
RUN go get github.com/go-redis/redis
RUN go get github.com/golang/protobuf/proto
COPY docker-entrypoint.sh /usr/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]
