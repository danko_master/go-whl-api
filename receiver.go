package main

import (
  "os"
  "log"
  "time"
  "strings"
  "github.com/streadway/amqp"
  "./graphhopper"
  "github.com/go-redis/redis"
  "github.com/golang/protobuf/proto"
  "./pbroute"
  "strconv"
)

func main() {
  // RabbitMQ
  conn, err := amqp.Dial(amqpConnectionString())
  failOnError(err, "Failed to connect to RabbitMQ")
  defer conn.Close()

  ch, err := conn.Channel()
  failOnError(err, "Failed to open a channel")
  defer ch.Close()

  rabbit_exchange := getEnv("RABBIT_EXCHANGE", "whl")

  err = ch.ExchangeDeclare(
    rabbit_exchange,                    // name
    "fanout",                           // type
    true,                               // durable
    false,                              // auto-deleted
    false,                              // internal
    false,                              // no-wait
    nil,                                // arguments
  )
  failOnError(err, "Failed to declare an exchange")

  q, err := ch.QueueDeclare(
    "",    // name
    false, // durable
    false, // delete when unused
    true,  // exclusive
    false, // no-wait
    nil,   // arguments
  )
  failOnError(err, "Failed to declare a queue")

  err = ch.QueueBind(
    q.Name,                           // queue name
    "",                               // routing key
    rabbit_exchange,                  // exchange
    false,
    nil)
  failOnError(err, "Failed to bind a queue")

  msgs, err := ch.Consume(
    q.Name, // queue
    "",     // consumer
    true,   // auto-ack
    false,  // exclusive
    false,  // no-local
    false,  // no-wait
    nil,    // args
  )
  failOnError(err, "Failed to register a consumer")

  forever := make(chan bool)

  // Redis
  redisDb, _ := strconv.Atoi(getEnv("REDIS_DB", "5"))
  redis_client := redis.NewClient(&redis.Options{
    Addr:     redisConnectionString(),
    Password: getEnv("REDIS_PASSWORD", ""), // no password set
    DB:       redisDb,  // use default DB
  })

  // Конфигурация для API геоданных
  config := graphhopper.NewConfiguration()
  graphApi := graphhopper.NewAPIClient(config)
  routingApi := graphApi.RoutingApi


  go func() {
    for d := range msgs {
      // Получаем маршрут из протобафа, декодируем его в нашу структуру
      way := new(pbroute.Way)
      err = proto.Unmarshal(d.Body, way)

      // Формируем точки для запроса во внешнее api
      points := []string{}
      for _, pointValue := range way.Points {
        var point strings.Builder
        point.WriteString(strconv.FormatFloat(pointValue.Lat, 'f', 6, 64))
        point.WriteString(",")
        point.WriteString(strconv.FormatFloat(pointValue.Long, 'f', 6, 64))
        points = append(points, point.String())
      }

      // Запрос информации из внешнего API
      geoData, _, err := routingApi.RouteGet(nil, points, true, getEnv("ROUTING_API_KEY", "LijBPDQGfu7Iiq80w3HzwB4RUDJbMbhs6BU0dEnn"), nil)

      // Кешируем результат в Редисе
      // Получаем длину маршрута в метрах
      way.Distance = geoData.Paths[0].Distance
      // Получаем продолжительность маршрута в миллисекундах
      way.Time     = geoData.Paths[0].Time

      if err != nil {
        log.Printf(" [!] Error before cache redis %s", err)
        way.Error    = err.Error()
      }
      cacheData, err := proto.Marshal(way)
      cacheExpires, _ := strconv.Atoi(getEnv("CACHE_EXPIRES", "600"))
      err = redis_client.Set(way.CacheKey, cacheData, time.Duration(cacheExpires)*time.Second).Err()

      if err != nil {
        log.Printf(" [!!!] Fatal error %s", err)
      }
      log.Printf(" [+] Cached result %s", way)
    }
  }()

  log.Printf(" [*] Waiting for input data. To exit press CTRL+C")
  <-forever
}

// Send error to log
func failOnError(err error, msg string) {
  if err != nil {
    log.Fatalf("%s: %s", msg, err)
  }
}

type Slice interface {
    Len() int
    Get(int) interface{}
}

// getEnv get key environment variable if exist otherwise return defalutValue
func getEnv(key, defaultValue string) string {
    value := os.Getenv(key)
    if len(value) == 0 {
        return defaultValue
    }
    return value
}

// Build connection string to RabbitMQ
func amqpConnectionString() string {
  s := []string{"amqp://",
                getEnv("RABBIT_USER", "guest"),
                ":",
                getEnv("RABBIT_PASSWORD", "guest"),
                "@",
                getEnv("RABBIT_HOST", "localhost"),
                ":",
                getEnv("RABBIT_PORT", "5672"),
                getEnv("RABBIT_VHOST", "/")};
  return strings.Join(s, "");
}

// Build connection string to Redis
func redisConnectionString() string {
  s := []string{getEnv("REDIS_HOST", "localhost"),
                ":",
                getEnv("REDIS_PORT", "6379")};
  return strings.Join(s, "");
}
