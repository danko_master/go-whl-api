// package pbroute;

// type Location struct {
//   Lat  float64
//   Long float64
// }

// type Way struct {
//   Points    []*Location
//   CacheKey  string
//   Distance  float64
//   Time      uint64
// }


package pbroute

import proto "github.com/golang/protobuf/proto"

var _ = proto.Marshal


type Location struct {
  Lat   float64  `protobuf:"fixed64,1,opt,name=lat" json:"lat,omitempty"`
  Long  float64 `protobuf:"fixed64,2,opt,name=long" json:"long,omitempty"`
}

// методы необходимы, чтобы структура соответствовала интерфейсу proto.Message
func (m *Location) Reset()         { *m = Location{} }
func (m *Location) String() string { return proto.CompactTextString(m) }
func (*Location) ProtoMessage()    {}


type Way struct {
  Points    []*Location  `protobuf:"bytes,1,rep,name=points" json:"points,omitempty"`
  CacheKey  string `protobuf:"bytes,2,opt,name=cache_key" json:"cache_key,omitempty"`
  Distance  float64 `protobuf:"fixed64,3,opt,name=distance" json:"distance,omitempty"`
  Time      int64 `protobuf:"fixed64,4,opt,name=time" json:"time,omitempty"`
  Error     string `protobuf:"bytes,5,opt,name=error" json:"error,omitempty"`
}

// методы необходимы, чтобы структура соответствовала интерфейсу proto.Message
func (m *Way) Reset()         { *m = Way{} }
func (m *Way) String() string { return proto.CompactTextString(m) }
func (*Way) ProtoMessage()    {}

func init() {
}
