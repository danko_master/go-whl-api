# API для расчета маршрута поездки по геоданным.

## Переменные окружения
1. Настройки шины данных
    * RABBIT_HOST, по умолчанию 'localhost'
    * RABBIT_PORT, по умолчанию 5672
    * RABBIT_USER, по умолчанию 'guest'
    * RABBIT_PASSWORD, по умолчанию 'guest'
    * RABBIT_VHOST, по умолчанию '/'
    * RABBIT_EXCHANGE, по умолчанию 'whl'

2. Настройки сервера кеширования
    * REDIS_HOST, по умолчанию 'localhost'
    * REDIS_PORT, по умолчанию 6379
    * REDIS_PASSWORD, по умолчанию без пароля
    * REDIS_DB, по умолчанию 5
    * CACHE_EXPIRES, срок жизни кеша (в секундах), по умолчанию 300 секунд

3. Настройки внешнего гео API
    * ROUTING_API_KEY, ключ для запроса во внешнее гео API, по умолчанию подставим ключ с официального сайта graphhopper.com LijBPDQGfu7Iiq80w3HzwB4RUDJbMbhs6BU0dEnn (применяется в OSM)


## Общее описание
Приложение принимает сообщение из шины и декодирует его в виде координат точек передвижения, затем производит отправку запроса во внешнее API для расчета расстояния, маршрута и времени передвижения. Готовый результат записывается в сервер кеширования.

## Запуск в Docker
1. `docker-compose build`
2. `docker-compose up`

## Схема
![Схема](https://bitbucket.org/danko_master/go-whl-api/raw/afbe15f37f65ec52a3daf4c4b7d2c213fda3f082/README_scheme.png)
